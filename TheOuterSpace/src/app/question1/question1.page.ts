import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-question1',
    templateUrl: './question1.page.html',
    styleUrls: ['./question1.page.scss'],
})
export class Question1Page implements OnInit {

    porcentaje1 = 1;
    myId = null;
    refreshId = null;

    constructor(private router: Router, private route: ActivatedRoute) {
    }

    changeValue1() {
        this.refreshId = setInterval(() => {
            this.porcentaje1 -= 0.033;
            if (this.porcentaje1 < 0) {
                this.router.navigateByUrl('/question2/' + this.myId);
                clearInterval(this.refreshId);
            }
        }, 1000);
    }

    question2(id) {
        clearInterval(this.refreshId);
        if (id === 1) {
            this.router.navigateByUrl('/question2/' + (this.myId + 1));
        } else {
            this.router.navigateByUrl('/question2/' + this.myId);
        }
    }

    ngOnInit() {
        this.changeValue1();
        // console.log(this.route.snapshot.paramMap.get('pun'));
        this.myId = Number(this.route.snapshot.paramMap.get('pun'));
    }

}
