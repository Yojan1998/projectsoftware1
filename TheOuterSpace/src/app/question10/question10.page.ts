import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-question10',
  templateUrl: './question10.page.html',
  styleUrls: ['./question10.page.scss'],
})
export class Question10Page implements OnInit {
  porcentaje10 = 1;
  myId = null;
  refreshId = null;

  changeValue() {
    this.refreshId = setInterval(() => {
      this.porcentaje10 -= 0.033;
      if (this.porcentaje10 < 0) {
        this.router.navigateByUrl('/results');
        clearInterval(this.refreshId);
      }
    }, 1000);
  }

  constructor(private router: Router, private route: ActivatedRoute) { }
  results(id) {
    clearInterval(this.refreshId);
    if (id === 1) {
      this.router.navigateByUrl('/results/' + (this.myId + 1));
    } else {
      this.router.navigateByUrl('/results/' + (this.myId ));
    }
  }

  ngOnInit() {
    this.changeValue();
    this.myId = Number(this.route.snapshot.paramMap.get('pun'));
  }

}
