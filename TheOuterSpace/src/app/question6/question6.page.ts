import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-question6',
  templateUrl: './question6.page.html',
  styleUrls: ['./question6.page.scss'],
})
export class Question6Page implements OnInit {
  porcentaje6 = 1;
  myId = null;
  refreshId = null;

  changeValue6() {
    this.refreshId = setInterval(() => {
      this.porcentaje6 -= 0.033;
      if (this.porcentaje6 < 0) {
        this.router.navigateByUrl('/question7/' + this.myId);
        clearInterval(this.refreshId);
      }
    }, 1000);
  }

  constructor(private router: Router, private route: ActivatedRoute) { }
  question7(id) {
    clearInterval(this.refreshId);
    if (id === 1) {
      this.router.navigateByUrl('/question7/' + (this.myId + 1));
    } else {
      this.router.navigateByUrl('/question7/' + this.myId);
    }
  }

  ngOnInit() {
    this.changeValue6();
    this.myId = Number(this.route.snapshot.paramMap.get('pun'));
  }

}
