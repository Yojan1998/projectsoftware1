import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.page.html',
  styleUrls: ['./questions.page.scss'],
})
export class QuestionsPage implements OnInit {

  porcentaje = 1;
  refreshId = null;

  changeValue() {
    this.refreshId = setInterval(() => {
      this.porcentaje -= 0.033;
      if (this.porcentaje < 0) {
        this.router.navigateByUrl('/question1/0');
        clearInterval(this.refreshId);
      }
    }, 1000);
  }

  constructor(private router: Router) { }

  question1(id) {
    clearInterval(this.refreshId);
    if (id === 1) {
      this.router.navigateByUrl('/question1/1');
    } else {
      this.router.navigateByUrl('/question1/0');
    }
  }

  ngOnInit() {
    this.changeValue();
  }

}
