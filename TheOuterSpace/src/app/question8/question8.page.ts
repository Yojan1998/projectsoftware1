import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-question8',
    templateUrl: './question8.page.html',
    styleUrls: ['./question8.page.scss'],
})
export class Question8Page implements OnInit {
    porcentaje8 = 1;
    myId = null;
    refreshId = null;

    changeValue8() {
        this.refreshId = setInterval(() => {
            this.porcentaje8 -= 0.033;
            if (this.porcentaje8 < 0) {
                this.router.navigateByUrl('/question9/' + this.myId);
                clearInterval(this.refreshId);
            }
        }, 1000);
    }

    constructor(private router: Router, private route: ActivatedRoute) {
    }

    question9(id) {
        clearInterval(this.refreshId);
        if (id === 1) {
            this.router.navigateByUrl('/question9/' + (this.myId + 1));
        } else {
            this.router.navigateByUrl('/question9/' + this.myId);
        }
    }

    ngOnInit() {
        this.changeValue8();
        this.myId = Number(this.route.snapshot.paramMap.get('pun'));
    }

}
