import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-question7',
  templateUrl: './question7.page.html',
  styleUrls: ['./question7.page.scss'],
})
export class Question7Page implements OnInit {
  porcentaje7 = 1;
  myId = null;
  refreshId = null;

  changeValue7() {
    this.refreshId = setInterval(() => {
      this.porcentaje7 -= 0.033;
      if (this.porcentaje7 < 0) {
        this.router.navigateByUrl('/question8/' + this.myId);
        clearInterval(this.refreshId);
      }
    }, 1000);
  }

  constructor(private router: Router, private route: ActivatedRoute) { }
  question8(id) {
    clearInterval(this.refreshId);
    if (id === 1) {
      this.router.navigateByUrl('/question8/' + (this.myId + 1));
    } else {
      this.router.navigateByUrl('/question8/' + this.myId);
    }
  }

  ngOnInit() {
    this.changeValue7();
    this.myId = Number(this.route.snapshot.paramMap.get('pun'));
  }

}
