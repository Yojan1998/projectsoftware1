import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-question2',
  templateUrl: './question2.page.html',
  styleUrls: ['./question2.page.scss'],
})
export class Question2Page implements OnInit {
  porcentaje2 = 1;
  myId = null;
  refreshId = null;

  constructor(private router: Router, private route: ActivatedRoute) { }

  question3(id) {
    clearInterval(this.refreshId);
    if (id === 1) {
      this.router.navigateByUrl('/question3/' + (this.myId + 1));
    } else {
      this.router.navigateByUrl('/question3/' + (this.myId ));
    }
  }

  changeValue2() {
    this.refreshId = setInterval(() => {
      this.porcentaje2 -= 0.033;
      if (this.porcentaje2 < 0) {
        this.router.navigateByUrl('/question3/' + this.myId);
        clearInterval(this.refreshId);
      }
    }, 1000);
  }

  ngOnInit() {
    this.changeValue2();
    // console.log(this.route.snapshot.paramMap.get('pun'));
    this.myId = Number(this.route.snapshot.paramMap.get('pun'));
  }

}
