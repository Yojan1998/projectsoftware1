import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-question3',
    templateUrl: './question3.page.html',
    styleUrls: ['./question3.page.scss'],
})
export class Question3Page implements OnInit {
    porcentaje3 = 1;
    myId = null;
    refreshId = null;

    changeValue3() {
        this.refreshId = setInterval(() => {
            this.porcentaje3 -= 0.033;
            if (this.porcentaje3 < 0) {
                this.router.navigateByUrl('/question4/' + this.myId);
                clearInterval(this.refreshId);
            }
        }, 1000);
    }

    constructor(private router: Router, private route: ActivatedRoute) {
    }

    question4(id) {
        clearInterval(this.refreshId);
        if (id === 1) {
            this.router.navigateByUrl('/question4/' + (this.myId + 1));
        } else {
            this.router.navigateByUrl('/question4/' + this.myId);
        }
    }

    ngOnInit() {
        this.changeValue3();
        this.myId = Number(this.route.snapshot.paramMap.get('pun'));
    }

}
