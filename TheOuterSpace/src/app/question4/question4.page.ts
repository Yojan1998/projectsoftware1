import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-question4',
    templateUrl: './question4.page.html',
    styleUrls: ['./question4.page.scss'],
})
export class Question4Page implements OnInit {
    porcentaje4 = 1;
    myId = null;
    refreshId = null;

    changeValue4() {
        this.refreshId = setInterval(() => {
            this.porcentaje4 -= 0.033;
            if (this.porcentaje4 < 0) {
                this.router.navigateByUrl('/question5/' + this.myId);
                clearInterval(this.refreshId);
            }
        }, 1000);
    }

    constructor(private router: Router, private route: ActivatedRoute) {
    }

    question5(id) {
        clearInterval(this.refreshId);
        if (id === 1) {
            this.router.navigateByUrl('/question5/' + (this.myId + 1));
        } else {
            this.router.navigateByUrl('/question5/' + this.myId);
        }
    }

    ngOnInit() {
        this.changeValue4();
        this.myId = Number(this.route.snapshot.paramMap.get('pun'));
    }

}
