import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'results/:pun',
    loadChildren: () => import('./results/results.module').then( m => m.ResultsPageModule)
  },
  {
    path: 'questions',
    loadChildren: () => import('./questions/questions.module').then( m => m.QuestionsPageModule)
  },
  {
    path: 'question1/:pun',
    loadChildren: () => import('./question1/question1.module').then( m => m.Question1PageModule)
  },
  {
    path: 'question2/:pun',
    loadChildren: () => import('./question2/question2.module').then( m => m.Question2PageModule)
  },
  {
    path: 'question3/:pun',
    loadChildren: () => import('./question3/question3.module').then( m => m.Question3PageModule)
  },
  {
    path: 'question4/:pun',
    loadChildren: () => import('./question4/question4.module').then( m => m.Question4PageModule)
  },
  {
    path: 'question5/:pun',
    loadChildren: () => import('./question5/question5.module').then( m => m.Question5PageModule)
  },
  {
    path: 'question6/:pun',
    loadChildren: () => import('./question6/question6.module').then( m => m.Question6PageModule)
  },
  {
    path: 'question7/:pun',
    loadChildren: () => import('./question7/question7.module').then( m => m.Question7PageModule)
  },
  {
    path: 'question8/:pun',
    loadChildren: () => import('./question8/question8.module').then( m => m.Question8PageModule)
  },
  {
    path: 'question9/:pun',
    loadChildren: () => import('./question9/question9.module').then( m => m.Question9PageModule)
  },
  {
    path: 'question10/:pun',
    loadChildren: () => import('./question10/question10.module').then( m => m.Question10PageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
