import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-question5',
  templateUrl: './question5.page.html',
  styleUrls: ['./question5.page.scss'],
})
export class Question5Page implements OnInit {
  porcentaje5 = 1;
  myId = null;
  refreshId = null;

  changeValue5() {
    this.refreshId = setInterval(() => {
      this.porcentaje5 -= 0.033;
      if (this.porcentaje5 < 0) {
        this.router.navigateByUrl('/question6/' + this.myId);
        clearInterval(this.refreshId);
      }
    }, 1000);
  }

  constructor(private router: Router, private route: ActivatedRoute) { }
  question6(id) {
    clearInterval(this.refreshId);
    if (id === 1) {
      this.router.navigateByUrl('/question6/' + (this.myId + 1));
    } else {
      this.router.navigateByUrl('/question6/' + this.myId);
    }
  }

  ngOnInit() {
    this.changeValue5();
    this.myId = Number(this.route.snapshot.paramMap.get('pun'));
  }

}
