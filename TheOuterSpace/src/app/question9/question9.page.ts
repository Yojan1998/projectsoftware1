import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-question9',
  templateUrl: './question9.page.html',
  styleUrls: ['./question9.page.scss'],
})
export class Question9Page implements OnInit {
  porcentaje9 = 1;
  myId = null;
  refreshId = null;

  changeValue10() {
    this.refreshId = setInterval(() => {
      this.porcentaje9 -= 0.033;
      if (this.porcentaje9 < 0) {
        this.router.navigateByUrl('/question10/' + this.myId);
        clearInterval(this.refreshId);
      }
    }, 1000);
  }

  constructor(private router: Router, private route: ActivatedRoute) { }
  question10(id) {
    clearInterval(this.refreshId);
    if (id === 1) {
      this.router.navigateByUrl('/question10/' + (this.myId + 1));
    } else {
      this.router.navigateByUrl('/question10/' + this.myId);
    }
  }

  ngOnInit() {
    this.changeValue10();
    this.myId = Number(this.route.snapshot.paramMap.get('pun'));
  }

}
