import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {
  pun = null;

  constructor(private router: Router, private route: ActivatedRoute) { }

  home() {
    this.router.navigateByUrl('/home');

  }
  questions() {
    this.router.navigateByUrl('/questions');
  }

  ngOnInit() {
    this.pun = Number(this.route.snapshot.paramMap.get('pun'));
  }

}
