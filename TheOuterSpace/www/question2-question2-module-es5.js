function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["question2-question2-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/question2/question2.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/question2/question2.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppQuestion2Question2PageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content padding class=\"background\" style=\"text-align: center\">\n    <!--  <div style=\"height: calc(6%);\"></div>-->\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"progress\">\n                    <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje2\"></ion-progress-bar>\n                </div>\n            </ion-col>\n            <br>\n            <br>\n            <br>\n            <ion-col size=\"12\">\n                <br>\n                <div>\n                    <img src=\"/assets/img/q3.jpg\"/>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <p style=\"justify-content: center\">\n                    What virtual telescope helped capture the first image of the black hole?</p>\n            </ion-col>\n\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question3(1)\">The Event Horizon Telescope (EHT)</ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question3(0)\">WorldWide Telescope</ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question3(0)\">Celestron</ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question3(0)\">Astronomical telescope</ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/question2/question2-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/question2/question2-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: Question2PageRoutingModule */

  /***/
  function srcAppQuestion2Question2RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question2PageRoutingModule", function () {
      return Question2PageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _question2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./question2.page */
    "./src/app/question2/question2.page.ts");

    var routes = [{
      path: '',
      component: _question2_page__WEBPACK_IMPORTED_MODULE_3__["Question2Page"]
    }];

    var Question2PageRoutingModule = function Question2PageRoutingModule() {
      _classCallCheck(this, Question2PageRoutingModule);
    };

    Question2PageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], Question2PageRoutingModule);
    /***/
  },

  /***/
  "./src/app/question2/question2.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/question2/question2.module.ts ***!
    \***********************************************/

  /*! exports provided: Question2PageModule */

  /***/
  function srcAppQuestion2Question2ModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question2PageModule", function () {
      return Question2PageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _question2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./question2-routing.module */
    "./src/app/question2/question2-routing.module.ts");
    /* harmony import */


    var _question2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./question2.page */
    "./src/app/question2/question2.page.ts");

    var Question2PageModule = function Question2PageModule() {
      _classCallCheck(this, Question2PageModule);
    };

    Question2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _question2_routing_module__WEBPACK_IMPORTED_MODULE_5__["Question2PageRoutingModule"]],
      declarations: [_question2_page__WEBPACK_IMPORTED_MODULE_6__["Question2Page"]]
    })], Question2PageModule);
    /***/
  },

  /***/
  "./src/app/question2/question2.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/question2/question2.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppQuestion2Question2PageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content {\n  --background: url('questions_background.jpg') 0 0/100% 100% no-repeat;\n}\n\nion-progress-bar {\n  height: 24px;\n  --buffer-background: white;\n  --progress-background: #0388ff;\n}\n\np {\n  font-size: 25px;\n  color: white;\n}\n\nion-button {\n  width: 310px;\n  height: 60px;\n  font-size: 15px;\n}\n\nimg {\n  width: 290px;\n  height: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95b2phbi9QeWNoYXJtUHJvamVjdHMvcHJvamVjdFNvZnR3YXJlMS9UaGVPdXRlclNwYWNlL3NyYy9hcHAvcXVlc3Rpb24yL3F1ZXN0aW9uMi5wYWdlLnNjc3MiLCJzcmMvYXBwL3F1ZXN0aW9uMi9xdWVzdGlvbjIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUVBQUE7QUNDSjs7QURHQTtFQUNJLFlBQUE7RUFDQSwwQkFBQTtFQUNBLDhCQUFBO0FDQUo7O0FESUE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQ0RKOztBRElBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDREo7O0FES0E7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQ0ZKIiwiZmlsZSI6InNyYy9hcHAvcXVlc3Rpb24yL3F1ZXN0aW9uMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuXG59XG5cbmlvbi1wcm9ncmVzcy1iYXJ7XG4gICAgaGVpZ2h0OiAyNHB4O1xuICAgIC0tYnVmZmVyLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIC0tcHJvZ3Jlc3MtYmFja2dyb3VuZDogIzAzODhmZjtcblxufVxuXG5we1xuICAgIGZvbnQtc2l6ZToyNXB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuaW9uLWJ1dHRvbntcbiAgICB3aWR0aDogMzEwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcblxufVxuXG5pbWd7XG4gICAgd2lkdGg6IDI5MHB4O1xuICAgIGhlaWdodDogMTUwcHg7XG59XG5cbiIsImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG5pb24tcHJvZ3Jlc3MtYmFyIHtcbiAgaGVpZ2h0OiAyNHB4O1xuICAtLWJ1ZmZlci1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1wcm9ncmVzcy1iYWNrZ3JvdW5kOiAjMDM4OGZmO1xufVxuXG5wIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbmlvbi1idXR0b24ge1xuICB3aWR0aDogMzEwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG5pbWcge1xuICB3aWR0aDogMjkwcHg7XG4gIGhlaWdodDogMTUwcHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/question2/question2.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/question2/question2.page.ts ***!
    \*********************************************/

  /*! exports provided: Question2Page */

  /***/
  function srcAppQuestion2Question2PageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question2Page", function () {
      return Question2Page;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var Question2Page = /*#__PURE__*/function () {
      function Question2Page(router, route) {
        _classCallCheck(this, Question2Page);

        this.router = router;
        this.route = route;
        this.porcentaje2 = 1;
        this.myId = null;
        this.refreshId = null;
      }

      _createClass(Question2Page, [{
        key: "question3",
        value: function question3(id) {
          clearInterval(this.refreshId);

          if (id === 1) {
            this.router.navigateByUrl('/question3/' + (this.myId + 1));
          } else {
            this.router.navigateByUrl('/question3/' + this.myId);
          }
        }
      }, {
        key: "changeValue2",
        value: function changeValue2() {
          var _this = this;

          this.refreshId = setInterval(function () {
            _this.porcentaje2 -= 0.033;

            if (_this.porcentaje2 < 0) {
              _this.router.navigateByUrl('/question3/' + _this.myId);

              clearInterval(_this.refreshId);
            }
          }, 1000);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.changeValue2(); // console.log(this.route.snapshot.paramMap.get('pun'));

          this.myId = Number(this.route.snapshot.paramMap.get('pun'));
        }
      }]);

      return Question2Page;
    }();

    Question2Page.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }];
    };

    Question2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-question2',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./question2.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/question2/question2.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./question2.page.scss */
      "./src/app/question2/question2.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])], Question2Page);
    /***/
  }
}]);
//# sourceMappingURL=question2-question2-module-es5.js.map