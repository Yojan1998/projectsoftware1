(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["question7-question7-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/question7/question7.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/question7/question7.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<ion-header>-->\n<!--    <ion-grid>-->\n<!--        <ion-row>-->\n<!--            <ion-col>-->\n<!--                <div class=\"progress\">-->\n<!--                    <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje7\"></ion-progress-bar>-->\n<!--                </div>-->\n<!--            </ion-col>-->\n<!--        </ion-row>-->\n<!--    </ion-grid>-->\n<!--</ion-header>-->\n\n<ion-content padding class=\"background\" style=\"text-align: center\">\n    <!--    <div style=\"height: calc(6%);\"></div>-->\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"progress\">\n                    <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje7\"></ion-progress-bar>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <img src=\"/assets/img/eso0848a.jpg\"/>\n                </div>\n            </ion-col>\n            <br>\n            <br>\n            <br>\n            <ion-col size=\"12\">\n                <p style=\"justify-content: center\">\n\n                    The image called NGC 2264 and the group of Christmas trees was taken on the date:</p>\n            </ion-col>\n\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question8(1)\">December 16, 2008\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">´\n                <ion-button (click)=\"question8(0)\">July 10, 2019</ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question8(0)\">March 5, 2019</ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question8(0)\">April 6, 2007</ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/question7/question7-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/question7/question7-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: Question7PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question7PageRoutingModule", function() { return Question7PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _question7_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./question7.page */ "./src/app/question7/question7.page.ts");




const routes = [
    {
        path: '',
        component: _question7_page__WEBPACK_IMPORTED_MODULE_3__["Question7Page"]
    }
];
let Question7PageRoutingModule = class Question7PageRoutingModule {
};
Question7PageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Question7PageRoutingModule);



/***/ }),

/***/ "./src/app/question7/question7.module.ts":
/*!***********************************************!*\
  !*** ./src/app/question7/question7.module.ts ***!
  \***********************************************/
/*! exports provided: Question7PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question7PageModule", function() { return Question7PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _question7_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./question7-routing.module */ "./src/app/question7/question7-routing.module.ts");
/* harmony import */ var _question7_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./question7.page */ "./src/app/question7/question7.page.ts");







let Question7PageModule = class Question7PageModule {
};
Question7PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _question7_routing_module__WEBPACK_IMPORTED_MODULE_5__["Question7PageRoutingModule"]
        ],
        declarations: [_question7_page__WEBPACK_IMPORTED_MODULE_6__["Question7Page"]]
    })
], Question7PageModule);



/***/ }),

/***/ "./src/app/question7/question7.page.scss":
/*!***********************************************!*\
  !*** ./src/app/question7/question7.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: url('questions_background.jpg') 0 0/100% 100% no-repeat;\n}\n\nion-progress-bar {\n  height: 24px;\n  --buffer-background: white;\n  --progress-background: #0388ff;\n}\n\np {\n  font-size: 25px;\n  color: white;\n}\n\nion-button {\n  width: 310px;\n  height: 60px;\n  font-size: 15px;\n}\n\nimg {\n  width: 290px;\n  height: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95b2phbi9QeWNoYXJtUHJvamVjdHMvcHJvamVjdFNvZnR3YXJlMS9UaGVPdXRlclNwYWNlL3NyYy9hcHAvcXVlc3Rpb243L3F1ZXN0aW9uNy5wYWdlLnNjc3MiLCJzcmMvYXBwL3F1ZXN0aW9uNy9xdWVzdGlvbjcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUVBQUE7QUNDSjs7QURHQTtFQUNJLFlBQUE7RUFDQSwwQkFBQTtFQUNBLDhCQUFBO0FDQUo7O0FESUE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQ0RKOztBRElBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDREo7O0FES0E7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQ0ZKIiwiZmlsZSI6InNyYy9hcHAvcXVlc3Rpb243L3F1ZXN0aW9uNy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuXG59XG5cbmlvbi1wcm9ncmVzcy1iYXJ7XG4gICAgaGVpZ2h0OiAyNHB4O1xuICAgIC0tYnVmZmVyLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIC0tcHJvZ3Jlc3MtYmFja2dyb3VuZDogIzAzODhmZjtcblxufVxuXG5we1xuICAgIGZvbnQtc2l6ZToyNXB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuaW9uLWJ1dHRvbntcbiAgICB3aWR0aDogMzEwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcblxufVxuXG5pbWd7XG4gICAgd2lkdGg6IDI5MHB4O1xuICAgIGhlaWdodDogMTUwcHg7XG59XG5cbiIsImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG5pb24tcHJvZ3Jlc3MtYmFyIHtcbiAgaGVpZ2h0OiAyNHB4O1xuICAtLWJ1ZmZlci1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1wcm9ncmVzcy1iYWNrZ3JvdW5kOiAjMDM4OGZmO1xufVxuXG5wIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbmlvbi1idXR0b24ge1xuICB3aWR0aDogMzEwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG5pbWcge1xuICB3aWR0aDogMjkwcHg7XG4gIGhlaWdodDogMTUwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/question7/question7.page.ts":
/*!*********************************************!*\
  !*** ./src/app/question7/question7.page.ts ***!
  \*********************************************/
/*! exports provided: Question7Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question7Page", function() { return Question7Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let Question7Page = class Question7Page {
    constructor(router, route) {
        this.router = router;
        this.route = route;
        this.porcentaje7 = 1;
        this.myId = null;
        this.refreshId = null;
    }
    changeValue7() {
        this.refreshId = setInterval(() => {
            this.porcentaje7 -= 0.033;
            if (this.porcentaje7 < 0) {
                this.router.navigateByUrl('/question8/' + this.myId);
                clearInterval(this.refreshId);
            }
        }, 1000);
    }
    question8(id) {
        clearInterval(this.refreshId);
        if (id === 1) {
            this.router.navigateByUrl('/question8/' + (this.myId + 1));
        }
        else {
            this.router.navigateByUrl('/question8/' + this.myId);
        }
    }
    ngOnInit() {
        this.changeValue7();
        this.myId = Number(this.route.snapshot.paramMap.get('pun'));
    }
};
Question7Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
Question7Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-question7',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./question7.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/question7/question7.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./question7.page.scss */ "./src/app/question7/question7.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
], Question7Page);



/***/ })

}]);
//# sourceMappingURL=question7-question7-module-es2015.js.map