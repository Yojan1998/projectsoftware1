function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["question4-question4-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/question4/question4.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/question4/question4.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppQuestion4Question4PageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content padding class=\"background\" style=\"text-align: center\">\n    <!--    <div style=\"height: calc(6%);\"></div>-->\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"progress\">\n                    <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje4\"></ion-progress-bar>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <img src=\"/assets/img/q5.jpg\"/>\n                </div>\n            </ion-col>\n            <br>\n            <br>\n            <br>\n            <ion-col size=\"12\">\n                <p style=\"justify-content: center\">Thor’s Helmet Nebula\n                    this photograph was taken in honor of : </p>\n            </ion-col>\n\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question5(1)\">\n                    ESO 50th Anniversary Celebration\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question5(0)\">\n                    Winner of the award <br>Tweet Your way to the VLTI\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question5(0)\">\n                    ESO's 15th anniversary celebration\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question5(0)\">\n                    ESO's 45th anniversary celebration\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/question4/question4-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/question4/question4-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: Question4PageRoutingModule */

  /***/
  function srcAppQuestion4Question4RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question4PageRoutingModule", function () {
      return Question4PageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _question4_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./question4.page */
    "./src/app/question4/question4.page.ts");

    var routes = [{
      path: '',
      component: _question4_page__WEBPACK_IMPORTED_MODULE_3__["Question4Page"]
    }];

    var Question4PageRoutingModule = function Question4PageRoutingModule() {
      _classCallCheck(this, Question4PageRoutingModule);
    };

    Question4PageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], Question4PageRoutingModule);
    /***/
  },

  /***/
  "./src/app/question4/question4.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/question4/question4.module.ts ***!
    \***********************************************/

  /*! exports provided: Question4PageModule */

  /***/
  function srcAppQuestion4Question4ModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question4PageModule", function () {
      return Question4PageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _question4_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./question4-routing.module */
    "./src/app/question4/question4-routing.module.ts");
    /* harmony import */


    var _question4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./question4.page */
    "./src/app/question4/question4.page.ts");

    var Question4PageModule = function Question4PageModule() {
      _classCallCheck(this, Question4PageModule);
    };

    Question4PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _question4_routing_module__WEBPACK_IMPORTED_MODULE_5__["Question4PageRoutingModule"]],
      declarations: [_question4_page__WEBPACK_IMPORTED_MODULE_6__["Question4Page"]]
    })], Question4PageModule);
    /***/
  },

  /***/
  "./src/app/question4/question4.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/question4/question4.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppQuestion4Question4PageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content {\n  --background: url('questions_background.jpg') 0 0/100% 100% no-repeat;\n}\n\nion-progress-bar {\n  height: 24px;\n  --buffer-background: white;\n  --progress-background: #0388ff;\n}\n\np {\n  font-size: 25px;\n  color: #ede6ff;\n}\n\nion-button {\n  width: 323px;\n  height: 60px;\n  font-size: 15px;\n}\n\nimg {\n  width: 290px;\n  height: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95b2phbi9QeWNoYXJtUHJvamVjdHMvcHJvamVjdFNvZnR3YXJlMS9UaGVPdXRlclNwYWNlL3NyYy9hcHAvcXVlc3Rpb240L3F1ZXN0aW9uNC5wYWdlLnNjc3MiLCJzcmMvYXBwL3F1ZXN0aW9uNC9xdWVzdGlvbjQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUVBQUE7QUNDSjs7QURHQTtFQUNJLFlBQUE7RUFDQSwwQkFBQTtFQUNBLDhCQUFBO0FDQUo7O0FESUE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQ0RKOztBRElBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDREo7O0FES0E7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQ0ZKIiwiZmlsZSI6InNyYy9hcHAvcXVlc3Rpb240L3F1ZXN0aW9uNC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuXG59XG5cbmlvbi1wcm9ncmVzcy1iYXJ7XG4gICAgaGVpZ2h0OiAyNHB4O1xuICAgIC0tYnVmZmVyLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIC0tcHJvZ3Jlc3MtYmFja2dyb3VuZDogIzAzODhmZjtcblxufVxuXG5we1xuICAgIGZvbnQtc2l6ZToyNXB4O1xuICAgIGNvbG9yOiAjZWRlNmZmO1xufVxuXG5pb24tYnV0dG9ue1xuICAgIHdpZHRoOiAzMjNweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuXG59XG5cbmltZ3tcbiAgICB3aWR0aDogMjkwcHg7XG4gICAgaGVpZ2h0OiAxNTBweDtcbn1cblxuIiwiaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1nL3F1ZXN0aW9uc19iYWNrZ3JvdW5kLmpwZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG59XG5cbmlvbi1wcm9ncmVzcy1iYXIge1xuICBoZWlnaHQ6IDI0cHg7XG4gIC0tYnVmZmVyLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLXByb2dyZXNzLWJhY2tncm91bmQ6ICMwMzg4ZmY7XG59XG5cbnAge1xuICBmb250LXNpemU6IDI1cHg7XG4gIGNvbG9yOiAjZWRlNmZmO1xufVxuXG5pb24tYnV0dG9uIHtcbiAgd2lkdGg6IDMyM3B4O1xuICBoZWlnaHQ6IDYwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDI5MHB4O1xuICBoZWlnaHQ6IDE1MHB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/question4/question4.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/question4/question4.page.ts ***!
    \*********************************************/

  /*! exports provided: Question4Page */

  /***/
  function srcAppQuestion4Question4PageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question4Page", function () {
      return Question4Page;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var Question4Page = /*#__PURE__*/function () {
      function Question4Page(router, route) {
        _classCallCheck(this, Question4Page);

        this.router = router;
        this.route = route;
        this.porcentaje4 = 1;
        this.myId = null;
        this.refreshId = null;
      }

      _createClass(Question4Page, [{
        key: "changeValue4",
        value: function changeValue4() {
          var _this = this;

          this.refreshId = setInterval(function () {
            _this.porcentaje4 -= 0.033;

            if (_this.porcentaje4 < 0) {
              _this.router.navigateByUrl('/question5/' + _this.myId);

              clearInterval(_this.refreshId);
            }
          }, 1000);
        }
      }, {
        key: "question5",
        value: function question5(id) {
          clearInterval(this.refreshId);

          if (id === 1) {
            this.router.navigateByUrl('/question5/' + (this.myId + 1));
          } else {
            this.router.navigateByUrl('/question5/' + this.myId);
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.changeValue4();
          this.myId = Number(this.route.snapshot.paramMap.get('pun'));
        }
      }]);

      return Question4Page;
    }();

    Question4Page.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }];
    };

    Question4Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-question4',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./question4.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/question4/question4.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./question4.page.scss */
      "./src/app/question4/question4.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])], Question4Page);
    /***/
  }
}]);
//# sourceMappingURL=question4-question4-module-es5.js.map