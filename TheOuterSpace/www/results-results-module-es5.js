function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["results-results-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/results/results.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/results/results.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppResultsResultsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content padding class=\"background\" style=\"text-align: center\">\n    <div style=\"height: calc(5%);\"></div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div>\n                    <img style=\"width: 200px; height: 200px;\" src=\"/assets/img/636X606.png\"/>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <h1>RESULTS</h1>\n                </div>\n                <div>\n                    <p>{{pun}}/10</p>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <ion-button (click)=\"questions()\" class=\"ion-text-uppercase\" id=\"button\"> try again</ion-button>\n                </div>\n            </ion-col>\n            <br>\n            <br>\n            <br>\n            <br>\n            <br>\n            <ion-col size=\"6\">\n                <div>\n                    <img (click)=\"home()\" style=\"width: 90px; height: 90px;\" src=\"/assets/img/home.png\"/>\n                </div>\n            </ion-col>\n            <ion-col size=\"6\">\n                <div>\n                    <img (click)=\"questions()\" style=\"width: 90px; height: 90px;\" src=\"/assets/img/tryagain.png\"/>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div class=\"creditos\">\n                    Credits :<br>\n                    <i>eso.org, Karen Gama, Yojan Hernandez,<br>\n                        Martha Avellaneda, Cristian Dulcey, <br>\n                        Emiraldo Lozano, Universidad de los llanos</i>\n                </div>\n            </ion-col>\n\n            <!--            <ion-col size=\"12\">-->\n            <!--                <div>-->\n            <!--                    <form>-->\n            <!--                        <input type='text' placeholder='Player Name'>-->\n            <!--                    </form>-->\n            <!--                </div>-->\n            <!--            </ion-col>-->\n            <!--            <ion-col size=\"12\">-->\n            <!--                <div>-->\n            <!--                    <ion-button class=\"ion-text-capitalize\" id=\"button\" color=\"success\"><p>Play</p></ion-button>-->\n            <!--                </div>-->\n            <!--            </ion-col>-->\n            <!--            <div>-->\n            <!--                <p>Creditos:</p><br>-->\n            <!--                <i>eso.org, Karen Gama, Yojan Hernandez,<br> Martha Avellaneda, Universidad de los llanos</i>-->\n            <!--            </div>-->\n        </ion-row>\n    </ion-grid>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/results/results-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/results/results-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: ResultsPageRoutingModule */

  /***/
  function srcAppResultsResultsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResultsPageRoutingModule", function () {
      return ResultsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _results_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./results.page */
    "./src/app/results/results.page.ts");

    var routes = [{
      path: '',
      component: _results_page__WEBPACK_IMPORTED_MODULE_3__["ResultsPage"]
    }];

    var ResultsPageRoutingModule = function ResultsPageRoutingModule() {
      _classCallCheck(this, ResultsPageRoutingModule);
    };

    ResultsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ResultsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/results/results.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/results/results.module.ts ***!
    \*******************************************/

  /*! exports provided: ResultsPageModule */

  /***/
  function srcAppResultsResultsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResultsPageModule", function () {
      return ResultsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _results_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./results-routing.module */
    "./src/app/results/results-routing.module.ts");
    /* harmony import */


    var _results_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./results.page */
    "./src/app/results/results.page.ts");

    var ResultsPageModule = function ResultsPageModule() {
      _classCallCheck(this, ResultsPageModule);
    };

    ResultsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _results_routing_module__WEBPACK_IMPORTED_MODULE_5__["ResultsPageRoutingModule"]],
      declarations: [_results_page__WEBPACK_IMPORTED_MODULE_6__["ResultsPage"]]
    })], ResultsPageModule);
    /***/
  },

  /***/
  "./src/app/results/results.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/results/results.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppResultsResultsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content {\n  --background: url('results.jpg') 0 0/100% 100% no-repeat;\n}\n\nion-button {\n  width: 220px;\n  height: 60px;\n  font-size: 20px;\n}\n\n#button {\n  --border-radius: 36px;\n}\n\np {\n  color: white;\n  font-size: 28px;\n}\n\nh1 {\n  font-size: 38px;\n  color: white;\n}\n\n.creditos {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95b2phbi9QeWNoYXJtUHJvamVjdHMvcHJvamVjdFNvZnR3YXJlMS9UaGVPdXRlclNwYWNlL3NyYy9hcHAvcmVzdWx0cy9yZXN1bHRzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcmVzdWx0cy9yZXN1bHRzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHdEQUFBO0FDQ0o7O0FER0E7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNBSjs7QURHQTtFQUNJLHFCQUFBO0FDQUo7O0FESUE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQ0RKOztBRElBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7QUNESjs7QURHQTtFQUNJLFlBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL3Jlc3VsdHMvcmVzdWx0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9yZXN1bHRzLmpwZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG5cbn1cblxuaW9uLWJ1dHRvbntcbiAgICB3aWR0aDogMjIwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuI2J1dHRvbntcbiAgICAtLWJvcmRlci1yYWRpdXM6IDM2cHg7XG5cbn1cblxucHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAyOHB4O1xufVxuXG5oMXtcbiAgICBmb250LXNpemU6IDM4cHg7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuLmNyZWRpdG9ze1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuIiwiaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1nL3Jlc3VsdHMuanBnKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuaW9uLWJ1dHRvbiB7XG4gIHdpZHRoOiAyMjBweDtcbiAgaGVpZ2h0OiA2MHB4O1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbiNidXR0b24ge1xuICAtLWJvcmRlci1yYWRpdXM6IDM2cHg7XG59XG5cbnAge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjhweDtcbn1cblxuaDEge1xuICBmb250LXNpemU6IDM4cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmNyZWRpdG9zIHtcbiAgY29sb3I6IHdoaXRlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/results/results.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/results/results.page.ts ***!
    \*****************************************/

  /*! exports provided: ResultsPage */

  /***/
  function srcAppResultsResultsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResultsPage", function () {
      return ResultsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var ResultsPage = /*#__PURE__*/function () {
      function ResultsPage(router, route) {
        _classCallCheck(this, ResultsPage);

        this.router = router;
        this.route = route;
        this.pun = null;
      }

      _createClass(ResultsPage, [{
        key: "home",
        value: function home() {
          this.router.navigateByUrl('/home');
        }
      }, {
        key: "questions",
        value: function questions() {
          this.router.navigateByUrl('/questions');
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.pun = Number(this.route.snapshot.paramMap.get('pun'));
        }
      }]);

      return ResultsPage;
    }();

    ResultsPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }];
    };

    ResultsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-results',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./results.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/results/results.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./results.page.scss */
      "./src/app/results/results.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])], ResultsPage);
    /***/
  }
}]);
//# sourceMappingURL=results-results-module-es5.js.map