(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["question10-question10-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/question10/question10.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/question10/question10.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<ion-header>-->\n<!--  <ion-grid>-->\n<!--    <ion-row>-->\n<!--      <ion-col>-->\n<!--        <div class=\"progress\">-->\n<!--          <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje10\" ></ion-progress-bar>-->\n<!--        </div>-->\n<!--      </ion-col>-->\n<!--    </ion-row>-->\n<!--  </ion-grid>-->\n<!--</ion-header>-->\n\n<ion-content padding class=\"background\" style=\"text-align: center\">\n    <!--    <div style=\"height: calc(6%);\"></div>-->\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"progress\">\n                    <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje10\"></ion-progress-bar>\n                </div>\n            </ion-col>\n            <br>\n            <br>\n            <br>\n            <ion-col size=\"12\">\n                <div>\n                    <img src=\"/assets/img/q10.jpg\"/>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <p style=\"justify-content: center\">\n                    The next photo is called:</p>\n            </ion-col>\n\n            <ion-col size=\"12\">\n                <ion-button (click)=\"results(1)\">Centaurus A</ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"results(0)\">\n                    Central black hole\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"results(0)\">\n                    The chair\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"results(0)\">Laboga</ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/question10/question10-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/question10/question10-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: Question10PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question10PageRoutingModule", function() { return Question10PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _question10_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./question10.page */ "./src/app/question10/question10.page.ts");




const routes = [
    {
        path: '',
        component: _question10_page__WEBPACK_IMPORTED_MODULE_3__["Question10Page"]
    }
];
let Question10PageRoutingModule = class Question10PageRoutingModule {
};
Question10PageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Question10PageRoutingModule);



/***/ }),

/***/ "./src/app/question10/question10.module.ts":
/*!*************************************************!*\
  !*** ./src/app/question10/question10.module.ts ***!
  \*************************************************/
/*! exports provided: Question10PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question10PageModule", function() { return Question10PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _question10_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./question10-routing.module */ "./src/app/question10/question10-routing.module.ts");
/* harmony import */ var _question10_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./question10.page */ "./src/app/question10/question10.page.ts");







let Question10PageModule = class Question10PageModule {
};
Question10PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _question10_routing_module__WEBPACK_IMPORTED_MODULE_5__["Question10PageRoutingModule"]
        ],
        declarations: [_question10_page__WEBPACK_IMPORTED_MODULE_6__["Question10Page"]]
    })
], Question10PageModule);



/***/ }),

/***/ "./src/app/question10/question10.page.scss":
/*!*************************************************!*\
  !*** ./src/app/question10/question10.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: url('questions_background.jpg') 0 0/100% 100% no-repeat;\n}\n\nion-progress-bar {\n  height: 24px;\n  --buffer-background: white;\n  --progress-background: #0388ff;\n}\n\np {\n  font-size: 25px;\n  color: white;\n}\n\nion-button {\n  width: 310px;\n  height: 60px;\n  font-size: 15px;\n}\n\nimg {\n  width: 290px;\n  height: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95b2phbi9QeWNoYXJtUHJvamVjdHMvcHJvamVjdFNvZnR3YXJlMS9UaGVPdXRlclNwYWNlL3NyYy9hcHAvcXVlc3Rpb24xMC9xdWVzdGlvbjEwLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcXVlc3Rpb24xMC9xdWVzdGlvbjEwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFFQUFBO0FDQ0o7O0FER0E7RUFDSSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSw4QkFBQTtBQ0FKOztBRElBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7QUNESjs7QURJQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ0RKOztBREtBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QUNGSiIsImZpbGUiOiJzcmMvYXBwL3F1ZXN0aW9uMTAvcXVlc3Rpb24xMC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuXG59XG5cbmlvbi1wcm9ncmVzcy1iYXJ7XG4gICAgaGVpZ2h0OiAyNHB4O1xuICAgIC0tYnVmZmVyLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIC0tcHJvZ3Jlc3MtYmFja2dyb3VuZDogIzAzODhmZjtcblxufVxuXG5we1xuICAgIGZvbnQtc2l6ZToyNXB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuaW9uLWJ1dHRvbntcbiAgICB3aWR0aDogMzEwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcblxufVxuXG5pbWd7XG4gICAgd2lkdGg6IDI5MHB4O1xuICAgIGhlaWdodDogMTUwcHg7XG59XG5cbiIsImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG5pb24tcHJvZ3Jlc3MtYmFyIHtcbiAgaGVpZ2h0OiAyNHB4O1xuICAtLWJ1ZmZlci1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1wcm9ncmVzcy1iYWNrZ3JvdW5kOiAjMDM4OGZmO1xufVxuXG5wIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbmlvbi1idXR0b24ge1xuICB3aWR0aDogMzEwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG5pbWcge1xuICB3aWR0aDogMjkwcHg7XG4gIGhlaWdodDogMTUwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/question10/question10.page.ts":
/*!***********************************************!*\
  !*** ./src/app/question10/question10.page.ts ***!
  \***********************************************/
/*! exports provided: Question10Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question10Page", function() { return Question10Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let Question10Page = class Question10Page {
    constructor(router, route) {
        this.router = router;
        this.route = route;
        this.porcentaje10 = 1;
        this.myId = null;
        this.refreshId = null;
    }
    changeValue() {
        this.refreshId = setInterval(() => {
            this.porcentaje10 -= 0.033;
            if (this.porcentaje10 < 0) {
                this.router.navigateByUrl('/results');
                clearInterval(this.refreshId);
            }
        }, 1000);
    }
    results(id) {
        clearInterval(this.refreshId);
        if (id === 1) {
            this.router.navigateByUrl('/results/' + (this.myId + 1));
        }
        else {
            this.router.navigateByUrl('/results/' + (this.myId));
        }
    }
    ngOnInit() {
        this.changeValue();
        this.myId = Number(this.route.snapshot.paramMap.get('pun'));
    }
};
Question10Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
Question10Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-question10',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./question10.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/question10/question10.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./question10.page.scss */ "./src/app/question10/question10.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
], Question10Page);



/***/ })

}]);
//# sourceMappingURL=question10-question10-module-es2015.js.map