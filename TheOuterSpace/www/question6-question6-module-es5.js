function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["question6-question6-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/question6/question6.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/question6/question6.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppQuestion6Question6PageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!--<ion-header>-->\n<!--  <ion-grid>-->\n<!--    <ion-row>-->\n<!--      <ion-col>-->\n<!--        <div class=\"progress\">-->\n<!--          <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje6\" ></ion-progress-bar>-->\n<!--        </div>-->\n<!--      </ion-col>-->\n<!--    </ion-row>-->\n<!--  </ion-grid>-->\n<!--</ion-header>-->\n\n<ion-content padding class=\"background\" style=\"text-align: center\">\n    <!--    <div style=\"height: calc(6%);\"></div>-->\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"progress\">\n                    <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje6\"></ion-progress-bar>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <img src=\"/assets/img/q7.jpg\"/>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <p style=\"justify-content: center\">\n                    The following image adopted the name of:</p>\n            </ion-col>\n\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question7(0)\">Vst</ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question7(0)\">Messier 78</ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question7(0)\">\n                    The nebula\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question7(1)\">\n                    The horse head nebula\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/question6/question6-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/question6/question6-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: Question6PageRoutingModule */

  /***/
  function srcAppQuestion6Question6RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question6PageRoutingModule", function () {
      return Question6PageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _question6_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./question6.page */
    "./src/app/question6/question6.page.ts");

    var routes = [{
      path: '',
      component: _question6_page__WEBPACK_IMPORTED_MODULE_3__["Question6Page"]
    }];

    var Question6PageRoutingModule = function Question6PageRoutingModule() {
      _classCallCheck(this, Question6PageRoutingModule);
    };

    Question6PageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], Question6PageRoutingModule);
    /***/
  },

  /***/
  "./src/app/question6/question6.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/question6/question6.module.ts ***!
    \***********************************************/

  /*! exports provided: Question6PageModule */

  /***/
  function srcAppQuestion6Question6ModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question6PageModule", function () {
      return Question6PageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _question6_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./question6-routing.module */
    "./src/app/question6/question6-routing.module.ts");
    /* harmony import */


    var _question6_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./question6.page */
    "./src/app/question6/question6.page.ts");

    var Question6PageModule = function Question6PageModule() {
      _classCallCheck(this, Question6PageModule);
    };

    Question6PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _question6_routing_module__WEBPACK_IMPORTED_MODULE_5__["Question6PageRoutingModule"]],
      declarations: [_question6_page__WEBPACK_IMPORTED_MODULE_6__["Question6Page"]]
    })], Question6PageModule);
    /***/
  },

  /***/
  "./src/app/question6/question6.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/question6/question6.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppQuestion6Question6PageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content {\n  --background: url('questions_background.jpg') 0 0/100% 100% no-repeat;\n}\n\nion-progress-bar {\n  height: 24px;\n  --buffer-background: white;\n  --progress-background: #0388ff;\n}\n\np {\n  font-size: 25px;\n  color: white;\n}\n\nion-button {\n  width: 310px;\n  height: 60px;\n  font-size: 15px;\n}\n\nimg {\n  width: 230px;\n  height: 230px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95b2phbi9QeWNoYXJtUHJvamVjdHMvcHJvamVjdFNvZnR3YXJlMS9UaGVPdXRlclNwYWNlL3NyYy9hcHAvcXVlc3Rpb242L3F1ZXN0aW9uNi5wYWdlLnNjc3MiLCJzcmMvYXBwL3F1ZXN0aW9uNi9xdWVzdGlvbjYucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUVBQUE7QUNDSjs7QURHQTtFQUNJLFlBQUE7RUFDQSwwQkFBQTtFQUNBLDhCQUFBO0FDQUo7O0FESUE7RUFDSSxlQUFBO0VBRUEsWUFBQTtBQ0ZKOztBREtBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDRko7O0FETUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQ0hKIiwiZmlsZSI6InNyYy9hcHAvcXVlc3Rpb242L3F1ZXN0aW9uNi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuXG59XG5cbmlvbi1wcm9ncmVzcy1iYXJ7XG4gICAgaGVpZ2h0OiAyNHB4O1xuICAgIC0tYnVmZmVyLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIC0tcHJvZ3Jlc3MtYmFja2dyb3VuZDogIzAzODhmZjtcblxufVxuXG5we1xuICAgIGZvbnQtc2l6ZToyNXB4O1xuICAgIC8vYmFja2dyb3VuZDogIzliOWI5YjtcbiAgICBjb2xvcjogd2hpdGU7XG59XG5cbmlvbi1idXR0b257XG4gICAgd2lkdGg6IDMxMHB4O1xuICAgIGhlaWdodDogNjBweDtcbiAgICBmb250LXNpemU6IDE1cHg7XG5cbn1cblxuaW1ne1xuICAgIHdpZHRoOiAyMzBweDtcbiAgICBoZWlnaHQ6IDIzMHB4O1xufVxuXG4iLCJpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvcXVlc3Rpb25zX2JhY2tncm91bmQuanBnKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuaW9uLXByb2dyZXNzLWJhciB7XG4gIGhlaWdodDogMjRweDtcbiAgLS1idWZmZXItYmFja2dyb3VuZDogd2hpdGU7XG4gIC0tcHJvZ3Jlc3MtYmFja2dyb3VuZDogIzAzODhmZjtcbn1cblxucCB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG5pb24tYnV0dG9uIHtcbiAgd2lkdGg6IDMxMHB4O1xuICBoZWlnaHQ6IDYwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDIzMHB4O1xuICBoZWlnaHQ6IDIzMHB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/question6/question6.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/question6/question6.page.ts ***!
    \*********************************************/

  /*! exports provided: Question6Page */

  /***/
  function srcAppQuestion6Question6PageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Question6Page", function () {
      return Question6Page;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var Question6Page = /*#__PURE__*/function () {
      function Question6Page(router, route) {
        _classCallCheck(this, Question6Page);

        this.router = router;
        this.route = route;
        this.porcentaje6 = 1;
        this.myId = null;
        this.refreshId = null;
      }

      _createClass(Question6Page, [{
        key: "changeValue6",
        value: function changeValue6() {
          var _this = this;

          this.refreshId = setInterval(function () {
            _this.porcentaje6 -= 0.033;

            if (_this.porcentaje6 < 0) {
              _this.router.navigateByUrl('/question7/' + _this.myId);

              clearInterval(_this.refreshId);
            }
          }, 1000);
        }
      }, {
        key: "question7",
        value: function question7(id) {
          clearInterval(this.refreshId);

          if (id === 1) {
            this.router.navigateByUrl('/question7/' + (this.myId + 1));
          } else {
            this.router.navigateByUrl('/question7/' + this.myId);
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.changeValue6();
          this.myId = Number(this.route.snapshot.paramMap.get('pun'));
        }
      }]);

      return Question6Page;
    }();

    Question6Page.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }];
    };

    Question6Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-question6',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./question6.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/question6/question6.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./question6.page.scss */
      "./src/app/question6/question6.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])], Question6Page);
    /***/
  }
}]);
//# sourceMappingURL=question6-question6-module-es5.js.map