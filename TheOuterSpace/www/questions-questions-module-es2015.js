(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["questions-questions-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/questions/questions.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/questions/questions.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<ion-header>-->\n<!--  <ion-grid>-->\n<!--    <ion-row>-->\n<!--      <ion-col>-->\n<!--        <div class=\"progress\">-->\n<!--          <ion-progress-bar color=\"tertiary\"  [value]=\"porcentaje\" ></ion-progress-bar>-->\n<!--        </div>-->\n<!--      </ion-col>-->\n<!--    </ion-row>-->\n<!--  </ion-grid>-->\n<!--</ion-header>-->\n\n<ion-content padding class=\"background\" style=\"text-align: center\">\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"progress\">\n                    <ion-progress-bar color=\"tertiary\" [value]=\"porcentaje\"></ion-progress-bar>\n                </div>\n            </ion-col>\n            <br>\n            <br>\n            <br>\n            <br>\n            <ion-col size=\"12\">\n                <p style=\"justify-content: center\"> What is the heart of the galaxy?</p>\n            </ion-col>\n\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question1(0)\">\n                    A fireball\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question1(0)\">\n                    A star\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question1(1)\">\n                    A supermassive black hole\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-button (click)=\"question1(0)\">\n                    A gravitational field\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/questions/questions-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/questions/questions-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: QuestionsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionsPageRoutingModule", function() { return QuestionsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _questions_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./questions.page */ "./src/app/questions/questions.page.ts");




const routes = [
    {
        path: '',
        component: _questions_page__WEBPACK_IMPORTED_MODULE_3__["QuestionsPage"]
    }
];
let QuestionsPageRoutingModule = class QuestionsPageRoutingModule {
};
QuestionsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], QuestionsPageRoutingModule);



/***/ }),

/***/ "./src/app/questions/questions.module.ts":
/*!***********************************************!*\
  !*** ./src/app/questions/questions.module.ts ***!
  \***********************************************/
/*! exports provided: QuestionsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionsPageModule", function() { return QuestionsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _questions_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./questions-routing.module */ "./src/app/questions/questions-routing.module.ts");
/* harmony import */ var _questions_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./questions.page */ "./src/app/questions/questions.page.ts");







let QuestionsPageModule = class QuestionsPageModule {
};
QuestionsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _questions_routing_module__WEBPACK_IMPORTED_MODULE_5__["QuestionsPageRoutingModule"]
        ],
        declarations: [_questions_page__WEBPACK_IMPORTED_MODULE_6__["QuestionsPage"]]
    })
], QuestionsPageModule);



/***/ }),

/***/ "./src/app/questions/questions.page.scss":
/*!***********************************************!*\
  !*** ./src/app/questions/questions.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: url('questions_background.jpg') 0 0/100% 100% no-repeat;\n}\n\nion-progress-bar {\n  height: 24px;\n  --buffer-background: white;\n  --progress-background: #0388ff;\n}\n\np {\n  font-size: 25px;\n  color: white;\n}\n\nion-button {\n  width: 310px;\n  height: 60px;\n  font-size: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95b2phbi9QeWNoYXJtUHJvamVjdHMvcHJvamVjdFNvZnR3YXJlMS9UaGVPdXRlclNwYWNlL3NyYy9hcHAvcXVlc3Rpb25zL3F1ZXN0aW9ucy5wYWdlLnNjc3MiLCJzcmMvYXBwL3F1ZXN0aW9ucy9xdWVzdGlvbnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUVBQUE7QUNDSjs7QURHQTtFQUNJLFlBQUE7RUFDQSwwQkFBQTtFQUNBLDhCQUFBO0FDQUo7O0FESUE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQ0RKOztBRElBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDREoiLCJmaWxlIjoic3JjL2FwcC9xdWVzdGlvbnMvcXVlc3Rpb25zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi9hc3NldHMvaW1nL3F1ZXN0aW9uc19iYWNrZ3JvdW5kLmpwZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG5cbn1cblxuaW9uLXByb2dyZXNzLWJhcntcbiAgICBoZWlnaHQ6IDI0cHg7XG4gICAgLS1idWZmZXItYmFja2dyb3VuZDogd2hpdGU7XG4gICAgLS1wcm9ncmVzcy1iYWNrZ3JvdW5kOiAjMDM4OGZmO1xuXG59XG5cbnB7XG4gICAgZm9udC1zaXplOjI1cHg7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG5pb24tYnV0dG9ue1xuICAgIHdpZHRoOiAzMTBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuXG59XG5cbiIsImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9xdWVzdGlvbnNfYmFja2dyb3VuZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xufVxuXG5pb24tcHJvZ3Jlc3MtYmFyIHtcbiAgaGVpZ2h0OiAyNHB4O1xuICAtLWJ1ZmZlci1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1wcm9ncmVzcy1iYWNrZ3JvdW5kOiAjMDM4OGZmO1xufVxuXG5wIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbmlvbi1idXR0b24ge1xuICB3aWR0aDogMzEwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/questions/questions.page.ts":
/*!*********************************************!*\
  !*** ./src/app/questions/questions.page.ts ***!
  \*********************************************/
/*! exports provided: QuestionsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionsPage", function() { return QuestionsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let QuestionsPage = class QuestionsPage {
    constructor(router) {
        this.router = router;
        this.porcentaje = 1;
        this.refreshId = null;
    }
    changeValue() {
        this.refreshId = setInterval(() => {
            this.porcentaje -= 0.033;
            if (this.porcentaje < 0) {
                this.router.navigateByUrl('/question1/0');
                clearInterval(this.refreshId);
            }
        }, 1000);
    }
    question1(id) {
        clearInterval(this.refreshId);
        if (id === 1) {
            this.router.navigateByUrl('/question1/1');
        }
        else {
            this.router.navigateByUrl('/question1/0');
        }
    }
    ngOnInit() {
        this.changeValue();
    }
};
QuestionsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
QuestionsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-questions',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./questions.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/questions/questions.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./questions.page.scss */ "./src/app/questions/questions.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], QuestionsPage);



/***/ })

}]);
//# sourceMappingURL=questions-questions-module-es2015.js.map