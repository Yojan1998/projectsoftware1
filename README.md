# README #
# TheOuterSpace

A continuacion, se muestran los requisitos que debemos tener encuenta antes de iniciar y como instalarlos.

desde una terminal:
1. Instalar Node.js
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install nodejs
```
2. Verificar que este instalado npm.
```
npm -version
```

3. Instalr Ionic Framework.
```
sudo npm install -g ionic
```
se instala de forma global para poder empezar el proyecto en cualquier lugar de nuestra computadora.

4. verificar que Ionioc quedo bien instalado.
```
ionic
```
en la consola debera salir toda la lista de comandos que se pueden usar

5. Instalar Capacitor
```
ionic integrations enable capacitor
````

6. verificar que capacitor esta bien instalado. 

````
npx cap --version
````
7. Ejecutar 

````
ionic buid
````
se debe hacer para que pueda ser envuelto el codigo de cada una de las plataformas

8. Añadir la plataforma Android
```
npx cap add android
```
9. Añadir la plataforma IOS

````
npx cap add ios
````
10. Desplegar

````
ionic serve
````


